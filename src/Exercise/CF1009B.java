package Exercise;


import java.util.Scanner;

/**
 * this is Codeforces 1009B - Div2 - WMinimum Ternary String [Greedy] (Arabic)
 * Created by mo3tamed on 8/6/18.
 *
 * algorithm:: as 1 can be replaced with 0 and 2  and  we can not change (2 and 0 and vice versa ) so
 * solution is to place all one's before 2 and after 0 at same time
 */
public class CF1009B {

    public static void main(String[] args) {


        Scanner in = new Scanner(System.in);
        //get input
        String s = in.next();

        int onesCount = 0;

        //get count of one's
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1')
                onesCount++;
        }
        //replace all 1's  to re,ove them
        s = s.replaceAll("1", "");
        //make StringBuilder to insert 1's later
        StringBuilder sb = new StringBuilder(s);
        //get index of 2 to insert 1's before it
        int indexOfTwo = s.indexOf('2');
        //guarantee it has 2 to add before it
        if (indexOfTwo != -1) {
            //case number contains 2
            for (int i = 0; i < onesCount; i++) {
                sb.insert(indexOfTwo, '1');
            }
        } else {
            //case number  not contains 2
            for (int i = 0; i < onesCount; i++) {
                sb.insert(s.length(), '1');
            }
        }

        System.out.println(sb.toString());

    }
}
